#!/bin/sh
export KUBECONFIG=$PWD/../../config/k3s.yaml

# Read environment variables
set -o allexport
source ../../.env

# use the main vm
vm_name=${vm_name}-1
IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
SUB_DOMAIN="${IP}.nip.io"

BRANCH=$(git symbolic-ref --short HEAD)
APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))-${BRANCH}

HOST="${APPLICATION_NAME}.${SUB_DOMAIN}"


http GET http://${HOST}/api/buddy/001

http GET http://${HOST}/api/buddy/002

http GET http://${HOST}/api/buddy/003