#!/bin/sh
export KUBECONFIG=$PWD/../../config/k3s.yaml
# Read environment variables
set -o allexport
source ../../.env
# use the main vm
vm_name=${vm_name}-1

NAMESPACE="default"
NB_REPLICAS=1

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
SUB_DOMAIN="${IP}.nip.io"

BRANCH=$(git symbolic-ref --short HEAD)
APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))-${BRANCH}

kubectl scale --replicas=${NB_REPLICAS} deploy ${APPLICATION_NAME} -n ${NAMESPACE}

export HOST="${APPLICATION_NAME}.${SUB_DOMAIN}"

echo "🌍 http://${HOST}"
