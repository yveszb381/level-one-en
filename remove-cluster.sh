#!/bin/sh

# Read environment variables
set -o allexport
source ./.env

multipass delete ${vm_name}-1
multipass delete ${vm_name}-2
multipass delete ${vm_name}-3
multipass purge

rm config/k3s.yaml
rm -rf monitoring/kube-prometheus
rm -rf apps/hello-js/.git
rm -rf apps/hey-js/.git
