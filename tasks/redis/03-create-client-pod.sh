#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml
NAMESPACE="demo"
kubectl apply -f pod.redis.client.yaml -n ${NAMESPACE}
