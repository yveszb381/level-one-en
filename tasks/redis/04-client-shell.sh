#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml
NAMESPACE="demo"
kubectl exec -n ${NAMESPACE} -it redis-client -- /bin/bash


# connection:
# redis-cli -h redis-mother (if you are in the same namespace as the database)

# To connect to redis-mother of database namespace
# from another namespace, use this notation:
# <service_name>.<namespace_name>

# connection:
# redis-cli -h redis-mother.database

# set firstname bob
# set lastname morane
# get firstname
# get lastname