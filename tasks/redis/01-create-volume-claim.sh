#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml
NAMESPACE="database"
kubectl apply -f pvc.redis.yaml -n ${NAMESPACE}
