#!/bin/sh

# Read environment variables
set -o allexport
source ./.env

multipass stop ${vm_name}-1
multipass stop ${vm_name}-2
multipass stop ${vm_name}-3


