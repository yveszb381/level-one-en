#!/bin/bash
export KUBECONFIG=$PWD/../config/k3s.yaml
# params: pod name, namespace
kubectl describe pods $1 -n $2

